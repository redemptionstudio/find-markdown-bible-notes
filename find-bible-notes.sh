#!/bin/bash
################################
# Information
TITLE="Find Markdown Bible Notes"
VERSION="0.0.1"
DATE="20221209"
AUTHOR="T. H. Wright"
LICENSE="GPLv3"
DESC="Finds all Bible passage files with notes in them by reporting a basic count."
#
################################

usage() {
cat << EOF
$TITLE, by $AUTHOR, v. $VERSION
  Usage: script <[options]>
  Options:
    -h   --help     Show this message
    -v   --version  Show TITLE, VERSION, AUTHOR, and DATE
    -V   --verbose  Turn on verbose mode
    -c   --config   Include a config file
EOF

}

verbose() {
	[[ $VERBOSE = true ]] && return 0 || return 1
}

run() {
	# BEGIN MAIN PROGRAM
	BIBLEHOME="$HOME/Documents/Bible"

	parse() {
        PATTERNBEGIN='###### [0-9+]'
        INPUTFILE="$1"
        tmpfile=$(mktemp /tmp/find-bible-notes.XXXXXX)
        # count lines following match
        # print count of lines
        # reset count
        # grab only counts greater than 2
        # skip first line
        awk "!/$PATTERNBEGIN/{count++}/$PATTERNBEGIN/{print count; count = 0}" "$INPUTFILE"  | grep -E "^[3-9]" | awk 'FNR > 1 {print $1}' > $tmpfile
        if [ -s $tmpfile ]; then
            echo $INPUTFILE
            cat $tmpfile
        fi
        rm $tmpfile
}

	export -f parse

	find $BIBLEHOME -type f -not -name "Index.md" -exec bash -c 'parse "$1"' bash {} \;

	# END MAIN PROGRAM
}

clean() {
	# BEGIN CLEAN UP
	:
	# END CLEAN UP
}

#if [ $# == 0 ]; then usage && exit; fi

# BEGIN OPTARGS
reset=true
for arg in "$@"
do
    if [ -n "$reset" ]; then
      unset reset
      set --      # this resets the "$@" array so we can rebuild it
    fi
    case "$arg" in
	--help)    set -- "$@" -h ;;
	--version) set -- "$@" -V ;;	
	--verbose) set -- "$@" -v ;;
	--config)  set -- "$@" -c ;;
	*)         set -- "$@" "$arg" ;;
    esac
done
OPTSTRING=':hVvc:t:aplT' #Available options

# First loop: set variable options
while getopts "$OPTSTRING" opt; do
        case $opt in
                v)  VERBOSE=true ;;
                c)  source $OPTARG ;;
                t)  TAGCHAR=$2; shift 2 ;;
        esac
done
OPTIND=1 # Reset the index.

# Second loop: determine user action

while getopts "$OPTSTRING" opt; do
    case $opt in
        h)  usage && exit ;;
        V)  echo "$TITLE, v. $VERSION by $AUTHOR. $DATE." && exit;;
        v)  VERBOSE=true ;;
        c)  source $OPTARG ;;
        \?) echo "$TITLE: -$OPTARG: undefined option." >&2 && usage >&2 && exit ;;
        :)  echo "$TITLE: -$OPTARG: missing argument." >&2 && usage >&2 && exit ;;
    esac
done
if [ $OPTIND -eq 1 ]; then
        verbose && echo "No options were passed.";
fi
shift $((OPTIND-1))
# END OPTARGS

# BEGIN EXECUTION
run;
clean;
# END EXECUTION

